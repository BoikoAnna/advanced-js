// Теоретичне питання

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототип - це об'єкт, від якого успадковують св-ва інші об'єкти.
// Наслідування реалізується конструкцією: objects
// Кожен object має внутрішнє посилання на інший object, що є його прототипом (prototype)
//  Цей прототипний об'єкт має прототип самого себе і так далі, поки не буде досягнуто null


// Для чого потрібно викликати super() у конструкторі класу-нащадка?
//слово super використовується в методі constructor для виклику конструктора батьківського класу

// Завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата).
// Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor({name, age, salary}) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
  }

  class Programmer extends Employee {
    constructor({ name, age, salary, lang }) {
      super({ name, age, salary });
      this._lang = lang;
    }
  
    get lang() {
      return this._lang;
    }
    set lang(value) {
      this._lang = value;
    }
    
  
    get salary() {
      return this._salary * 3;
    }
  }
  
  const ann = new Programmer({
    name: "Anna",
    age: 32,
    salary: 1500,
    lang: ["R", "JavaScript.", "HTML and CSS"],
  });
  
  const sasha = new Programmer({
    name: "Oleksandr",
    age: 33,
    salary: 5000,
    lang: ["C Sharp", "Python", "Delphi"],
  });
  
  console.log(ann);
  console.log(ann.salary);
  console.log(sasha);
  console.log(sasha.salary);

